## Instalar NodeJS version LTS
https://nodejs.org/es/

## Clonar el proyecto

## instalar dependencias
npm install

## correr el servidor desde consola
node index.js

## setear la IP correcta en la App.
Si usan emulador puede ser:
http://localhost:4000/
si usan red:
http://192.168.1.100:4000/
