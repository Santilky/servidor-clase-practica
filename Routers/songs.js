const router = require('express').Router()
let songs = [
    {
        id: 0,
        band: 'Metallica',
        song: 'Nothing else Matters',
        duration: '6:38'
    },
    {
        id: 1,
        band: 'Lady Gaga',
        song: 'Poker Face',
        duration: '4:32'
    },
    {
        id: 2,
        band: 'Elle King',
        song: "Ex's and Oh's",
        duration: '2:51'
    },
    {
        id: 3,
        band: 'Andrea Celeste',
        song: 'My Personal Hell',
        duration: '6:38'
    }
]
router.get('/',(req, res) => {
    res.json({
        results: songs
    })
})
router.get('/:id', (req, res) => {
    const id = req.params.id
    res.json({
        result: songs.find(song => song.id == id)
    })
})
module.exports = router