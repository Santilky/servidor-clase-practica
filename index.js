const Express = require('express')
const app = Express()
const songsRouter = require('./Routers/songs')
const port = 4000
app.use(Express.json())
app.use('/songs', songsRouter)
app.post('/ping', (req, res) => {
    res.json(req.body)
})
app.listen(port, () => {
    console.log(`Listening port ${port}`)
})